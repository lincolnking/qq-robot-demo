package com.lincoln.config;

import com.lincolnXmypcqq.cmds.framework.RobotCmdFactory;
import com.lincolnXmypcqq.config.StartRun;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 启动服务器前执行
 * Date:2017/8/21.
 * Time:15:36
 * @author lincoln
 */
@Component()
@Service
@Order(value = 1)
public class RunStart extends StartRun {
    private static ApplicationContext applicationContext = null;

    @Override
    public void run(String... args) throws Exception {
        super.run(args);
        RobotCmdFactory.scanClass("com.lincoln.cmds");
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (applicationContext == null) {
            applicationContext = contextRefreshedEvent.getApplicationContext();
        }
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
