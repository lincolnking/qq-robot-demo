package com.lincoln.entity;

import com.lincoln.entity.base.BaseEnum;
import com.lincoln.entity.base.BaseEnumConverter;

/**
 * @description
 * //* .参数 参_消息类型,msgType, 整数型, , 信息唯一标识-1 未定义事件 1 好友信息 2,群信息 3,讨论组信息 4,临时会话
 * // 1001,被添加好友 1002,好友在线状态改变 1003 被删除好友 1004 签名变更 2001 某人申请加入群 2002 某人被邀请加入群
 * // 2003 我被邀请加入群 2005 某人被批准加入了群 2006 某人退出群  2007 某人被管理移除群 2008 某群被解散 2009 某人成为管理员
 * // 2010 某人被取消管理员 2011 群名片变动 2012 群名变动//暂未解析 2013 群公告变动
 * @author ziyao.peng01@ucarinc.com
 * @date
 * @param
 * @return
 */
public enum MsgType implements BaseEnum<Integer> {
    /**
     * 未定义事件
     */
    NONE(-1, "未定义事件"),
    FRIEND(1, "好友信息"),
    QUN(2, "群信息"),
    GROUP(3, "讨论组信息"),
    TEMP(4, "临时会话"),
    ADDFRIEND(1001, "被添加好友"),
    CHANGESTATE(1002, "好友在线状态改变"),
    DELETEFRIEND(1003, "被删除好友"),
    CHANGESIGN(1004, "签名变更"),
    ADDQUN(2001, "某人申请加入群"),
    INVITEQUN(2002, "某人被邀请加入群"),
    INVITEDQUN(2003, "我被邀请加入群"),
    ADDEDQUN(2005, "某人被批准加入了群"),
    EXITQUN(2006, "某人退出群"),
    REMOVEQUN(2007, "某人被管理移除群"),
    DELETEQUN(2008, "某群被解散"),
    MANAGEQUN(2009, "某人成为管理员"),
    NOMANAGEQUN(2010, "某人被取消管理员"),
    CHANGEQUNSIGN(2011, "群名片变动"),
    CHANGEQUNNAME(2012, "群名变动"),
    CHANGEQUNNOTICE(2013, "群公告变动");
    int value;
    String name;

    MsgType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Convert extends BaseEnumConverter<MsgType, Integer> {

    }
}
