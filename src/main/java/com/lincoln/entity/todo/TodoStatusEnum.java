package com.lincoln.entity.todo;

import com.lincoln.entity.base.BaseEnum;
import com.lincoln.entity.base.BaseEnumConverter;

/**
 * @description 待办事项状态
 * @author ziyao.peng01@ucarinc.com
 * @date 2018/11/29 15:30
 */
public enum TodoStatusEnum implements BaseEnum<Integer> {
    /**
     * @description 未开始
     */
    WATI(0, "未开始"),
    /**
     * @description 开始
     */
    BEGIN(1, "开始"),
    /**
     * @description 结束
     */
    END(2, "结束");
    int value;
    String name;

    TodoStatusEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Convert extends BaseEnumConverter<TodoStatusEnum, Integer> {

    }
}
