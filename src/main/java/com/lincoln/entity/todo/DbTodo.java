package com.lincoln.entity.todo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lincoln.entity.base.BaseEntity;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.entity.user.DbUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Entity(name = "t_todo")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})//转json忽略参数
public class DbTodo extends BaseEntity {
    //创建者id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    DbUser user;

    //群组id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    DbGroup group;

    /**
     * 约定开始时间
     */
    @Column(name = "start_time")
    protected Date startTime;

    /**
     * 约定结束时间
     */
    @Column(name = "end_time")
    protected Date endTime;

    /**
     * 提示语
     */
    @Column(name = "title",columnDefinition = "varchar(300)",length = 300,nullable = true)
    protected String title;

    //描述
    @Column(name = "descriptor",columnDefinition = "varchar(300)",length = 300,nullable = false)
    protected String descriptor;

    //提示语
    @Column(name = "hint",columnDefinition = "varchar(300)",length = 300,nullable = true)
    protected String hint;

    //人数
    @Column(name = "number",columnDefinition = "int(5)",length = 5,nullable = true)
    protected Integer number;

    //状态:0未开始,1正在进行,2已结束
    @Convert(converter = TodoStatusEnum.Convert.class)
    @Column(name = "status",columnDefinition = "int(1) default 0",length = 1,nullable = true)
    protected TodoStatusEnum status;

    @Transient
    public String getStartTimeText(){
        if(this.startTime == null){
            return "";
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.startTime);
    }
    @Transient
    public String getEndTimeText(){
        if(this.endTime == null){
            return "";
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.endTime);
    }
}
