package com.lincoln.entity.todo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lincoln.entity.base.BaseEntity;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.entity.user.DbUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "t_todo_user")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})//转json忽略参数
public class DbTodoUser extends BaseEntity {
    //创建者id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    DbUser user;

    //创建者id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "todo_id", nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    DbTodo todo;


}
