package com.lincoln.entity.user;

import com.lincoln.entity.base.BaseEnum;
import com.lincoln.entity.base.BaseEnumConverter;

/**
 * @description 群组类型
 * @author ziyao.peng01@ucarinc.com
 * @date 2018/11/29 15:30
 */
public enum GroupTypeEnum implements BaseEnum<Integer> {
    /**
     * @description 群
     */
    QUN(0, "群"),
    /**
     * @description 讨论组
     */
    GROUP(1, "讨论组");
    /**
     * @description 值
     */
    int value;
    /**
     * @description 名称
     */
    String name;

    GroupTypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Convert extends BaseEnumConverter<GroupTypeEnum, Integer> {

    }
}
