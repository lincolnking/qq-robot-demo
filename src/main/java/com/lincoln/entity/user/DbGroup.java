package com.lincoln.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lincoln.entity.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;

@Data
@Entity(name = "t_group")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})//转json忽略参数
public class DbGroup extends BaseEntity {
    //用户名
    @Column(name="group_no",columnDefinition = "varchar(30)",length = 30,nullable = false)
    String groupNo;

    //是否是讨论组,0是1群
    @Convert(converter = GroupTypeEnum.Convert.class)
    @Column(name="group_type",columnDefinition = "int(1) default 0",length = 1,nullable = false)
    GroupTypeEnum groupType;
}
