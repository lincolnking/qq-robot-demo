package com.lincoln.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lincoln.entity.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;

@Data
@Entity(name = "t_user")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})//转json忽略参数
public class DbUser extends BaseEntity {
    //用户名
    @Column(name="qq",columnDefinition = "varchar(30)",length = 30,nullable = false)
    String qq;

    //身份
    @Convert(converter = UserIdentityEnum.Convert.class)
    @Column(name="identity",columnDefinition = "int(1) default 0",length = 1,nullable = false)
    UserIdentityEnum identity;
}
