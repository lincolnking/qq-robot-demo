package com.lincoln.entity.user;

import com.lincoln.entity.base.BaseEnum;
import com.lincoln.entity.base.BaseEnumConverter;

/**
 * @description 用户类型
 * @author ziyao.peng01@ucarinc.com
 * @date 2018/11/29 15:30
 */
public enum UserIdentityEnum implements BaseEnum<Integer> {
    /**
     * @description 普通用户
     */
    USER(0, "普通用户"),
    /**
     * @description 机器人
     */
    ROBOT(1, "机器人");
    int value;
    String name;

    UserIdentityEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Convert extends BaseEnumConverter<UserIdentityEnum, Integer> {

    }
}
