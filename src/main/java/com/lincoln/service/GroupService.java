package com.lincoln.service;

import com.lincoln.entity.user.DbGroup;
import com.lincoln.repository.GroupRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class GroupService extends BaseService<DbGroup, GroupRepository> {

    /**
     * 建立分页排序请求
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageRequest buildPageRequest(int pageNo, int pageSize, Sort.Order... orders) {
        Sort sort = Sort.by(orders);
        return PageRequest.of(pageNo, pageSize, sort);
    }

    @Override
    public Predicate createPredicate(Root<DbGroup> root, CriteriaBuilder cb, Map<String, Object> params) {
        return super.createPredicate(root,cb,params);
    }

    /**
     * 通过QQ查询
     * @param qq
     * @return
     */
    public DbGroup findByQQ(String groupNo) {
        return findByUniqueForOne("groupNo", groupNo);
    }
}
