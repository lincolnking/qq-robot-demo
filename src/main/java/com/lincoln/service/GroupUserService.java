package com.lincoln.service;

import com.lincoln.entity.user.DbGroupUser;
import com.lincoln.entity.user.GroupTypeEnum;
import com.lincoln.entity.user.UserIdentityEnum;
import com.lincoln.repository.GroupUserRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class GroupUserService extends BaseService<DbGroupUser, GroupUserRepository> {

    /**
     * 建立分页排序请求
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageRequest buildPageRequest(int pageNo, int pageSize, Sort.Order... orders) {
        Sort sort = Sort.by(orders);
        return PageRequest.of(pageNo, pageSize, sort);
    }

    @Override
    public Predicate createPredicate(Root<DbGroupUser> root, CriteriaBuilder cb, Map<String, Object> params) {
        Predicate result = cb.and();
        if (params.get("userId") != null) {
            Predicate p = cb.equal(root.get("user").get("id").as(Long.class), params.get("userId"));
            result = cb.and(result, p);
        }
        if (params.get("qq") != null) {
            Predicate p = cb.equal(root.get("user").get("qq").as(String.class), params.get("qq"));
            result = cb.and(result, p);
        }
        if (params.get("groupId") != null) {
            Predicate p = cb.equal(root.get("group").get("id").as(Long.class), params.get("groupId"));
            result = cb.and(result, p);
        }
        if (params.get("group") != null) {
            Predicate p = cb.equal(root.get("group").get("groupNo").as(String.class), params.get("groupId"));
            result = cb.and(result, p);
        }
        if (params.get("type") != null) {
            Predicate p = cb.equal(root.get("group").get("groupType").as(Integer.class), params.get("type"));
            result = cb.and(result, p);
        }
        if (params.get("identity") != null) {
            Predicate p = cb.equal(root.get("user").get("identity").as(Integer.class), params.get("identity"));
            result = cb.and(result, p);
        }
        return result;
    }

    public DbGroupUser findByQQAndGroupNo(String qq, String groupNo, int type) {
        Map<String, Object> params = new HashMap<>();
        params.put("qq", qq);
        params.put("group", groupNo);
        params.put("type", type);
        List<DbGroupUser> list = findList(params);
        if (list == null || list.size() <= 0) {
            return null;
        }
        return list.get(0);
    }

    public DbGroupUser findByUserIdAndGroupId(long userId,long groupId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("groupId", groupId);
        List<DbGroupUser> list = findList(params);
        if (list == null || list.size() <= 0) {
            return null;
        }
        return list.get(0);
    }

    public List<DbGroupUser> findGroupRobot(long groupId) {
        Map<String, Object> params = new HashMap<>();
        params.put("groupId", groupId);
        params.put("identity", UserIdentityEnum.ROBOT.getValue());
        List<DbGroupUser> list = findList(params);
        return list;
    }
}
