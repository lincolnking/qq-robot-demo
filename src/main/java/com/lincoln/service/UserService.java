package com.lincoln.service;

import com.lincoln.entity.user.DbUser;
import com.lincoln.repository.UserRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class UserService extends BaseService<DbUser, UserRepository> {

    /**
     * 建立分页排序请求
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageRequest buildPageRequest(int pageNo, int pageSize, Sort.Order... orders) {
        Sort sort = Sort.by(orders);
        return PageRequest.of(pageNo, pageSize, sort);
    }

    @Override
    public Predicate createPredicate(Root<DbUser> root, CriteriaBuilder cb, Map<String, Object> params) {
        Predicate result = cb.and();
        if (params.get("qqEQ") != null) {
            Predicate p = cb.like(root.get("qq").as(String.class), "%" + params.get("qqEQ") + "%");
            result = cb.and(result, p);
        }
        if (params.get("qq") != null) {
            Predicate p = cb.equal(root.get("qq").as(String.class), params.get("qq"));
            result = cb.and(result, p);
        }
        if (params.get("robot") != null) {
            Predicate p = cb.equal(root.get("robot").as(Integer.class), params.get("robot"));
            result = cb.and(result, p);
        }
        return result;
    }

    /**
     * 通过QQ查询
     * @param qq
     * @return
     */
    public DbUser findByQQ(String qq) {
        return findByUniqueForOne("qq", qq);
    }
}
