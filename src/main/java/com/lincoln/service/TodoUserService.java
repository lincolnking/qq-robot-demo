package com.lincoln.service;

import com.lincoln.entity.todo.DbTodo;
import com.lincoln.entity.todo.DbTodoUser;
import com.lincoln.entity.todo.TodoStatusEnum;
import com.lincoln.entity.user.DbUser;
import com.lincoln.repository.TodoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class TodoUserService extends BaseService<DbTodoUser, TodoUserRepository> {

    @Autowired
    TodoService todoService;

    /**
     * 建立分页排序请求
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageRequest buildPageRequest(int pageNo, int pageSize, Sort.Order... orders) {
        Sort sort = Sort.by(orders);
        return PageRequest.of(pageNo, pageSize, sort);
    }

    @Override
    public Predicate createPredicate(Root<DbTodoUser> root, CriteriaBuilder cb, Map<String, Object> params) {
        Predicate result = cb.and();
        if (params.get("userId") != null) {
            Predicate p = cb.equal(root.get("user").get("id").as(Long.class), params.get("userId"));
            result = cb.and(result, p);
        }
        if (params.get("qq") != null) {
            Predicate p = cb.equal(root.get("user").get("qq").as(String.class), params.get("qq"));
            result = cb.and(result, p);
        }
        if (params.get("todoId") != null) {
            Predicate p = cb.equal(root.get("todo").get("id").as(Long.class), params.get("todoId"));
            result = cb.and(result, p);
        }
        if (params.get("groupId") != null) {
            Predicate p = cb.equal(root.get("todo").get("group").get("id").as(Long.class), params.get("groupId"));
            result = cb.and(result, p);
        }
        if (params.get("todoUserId") != null) {
            Predicate p = cb.equal(root.get("todo").get("user").get("id").as(Long.class), params.get("todoUserId"));
            result = cb.and(result, p);
        }
        if (params.get("status") != null) {
            Predicate p = cb.equal(root.get("todo").get("status").get("value").as(Integer.class), params.get("status"));
            result = cb.and(result, p);
        }
        if (params.get("statusNoEq") != null) {
            Predicate p = cb.notEqual(root.get("todo").get("status").as(Integer.class), params.get("statusNoEq"));
            result = cb.and(result, p);
        }
        if (params.get("endTimeLe") != null) {
            Predicate p = cb.lessThanOrEqualTo(root.get("todo").get("endTime").as(Date.class), (Date) params.get("endTimeLe"));
            result = cb.and(result, p);
        }
        return result;
    }

    public DbTodoUser findByQQAndTodoId(String qq, long todoId) {
        Map<String, Object> params = new HashMap<>();
        params.put("qq", qq);
        params.put("todoId", todoId);
        List<DbTodoUser> list = findList(params);
        if (list == null || list.size() <= 0) {
            return null;
        }
        return list.get(0);
    }

    public DbTodoUser findByUserIdAndTodoId(long userId, long todoId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("todoId", todoId);
        List<DbTodoUser> list = findList(params);
        if (list == null || list.size() <= 0) {
            return null;
        }
        return list.get(0);
    }

    public List<DbTodoUser> findByTodoId(long todoId) {
        Map<String, Object> params = new HashMap<>();
        params.put("todoId", todoId);
        List<DbTodoUser> list = findList(params);
        return list;
    }

    /**
     * @description 查询用户所有
     * @author ziyao.peng01@ucarinc.com
     * @date 2018/11/28 16:44
     * @param userId, status
     * @return java.util.List<DbTodoUser>
     */
    public List<DbTodoUser> findByUserId(long userId, Integer status) {
        return findByUserIdAndGroupId(userId,null,status);
    }

    /**
     * @description 查询用户在某个群所有
     * @author ziyao.peng01@ucarinc.com
     * @date 2018/11/28 16:44
     * @param userId, status
     * @return java.util.List<DbTodoUser>
     */
    public List<DbTodoUser> findByUserIdAndGroupId(long userId,Long groupId, Integer status) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        if(groupId != null){
            params.put("groupId", groupId);
        }
        if (status != null) {
            if (status == 3) {
                params.put("statusNoEq", TodoStatusEnum.END.getValue());
                params.put("endTimeLe", new Date());
            } else {
                params.put("status", status);
            }
        }
        List<DbTodoUser> list = findList(params);
        return list;
    }

    public String joinTodo(DbUser user, DbTodo todo) {
        DbTodoUser todoUser = findByUserIdAndTodoId(user.getId(), todo.getId());
        if (todoUser != null) {
            return "你已经在里面了";
        } else {
            todoUser = new DbTodoUser();
            todoUser.setTodo(todo);
            todoUser.setUser(user);
            todoUser = save(todoUser);
            todo.setNumber(todo.getNumber() - 1);
            todo = todoService.save(todo);
            return "\"" + todo.getTitle() + "\"加入成功";
        }
    }

    public boolean escTodo(DbTodoUser todoUser) {
        if (todoUser != null) {
            DbTodo todo = todoUser.getTodo();
            if (todo != null) {
                todo.setNumber(todo.getNumber() + 1);
                delete(todoUser);
                todoService.save(todo);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 查找某个用户加入的某个群的某人的未结束的活动
     *
     * @param userId     某个用户
     * @param todoUserId 某人
     * @param groupId    某个群
     * @return
     */
    public List<DbTodoUser> findByUserIdAndGroupIdAndTodoUserId(long userId, long todoUserId, long groupId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("groupId", groupId);
        params.put("todoUserId", todoUserId);
        params.put("statusNoEq", TodoStatusEnum.END.getValue());
        return findList(params);
    }
}
