package com.lincoln.cmds;

import cn.xsshome.taip.vision.TAipVision;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lincoln.entity.MsgType;
import com.lincoln.utils.ImageUtil;
import com.lincoln.utils.QQAiUtil;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.Msg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.RobotDeal;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 鉴黄
 *
 * @author ziyao.peng01@ucarinc.com
 * @description 鉴黄
 * @date 2018/11/27 10:26
 **/
@Msg(priority = 1)
public class VisionPorn {
    private static final Pattern PIC_PATTERN = Pattern.compile("pzypzypzypzy(.*?)pzypzypzypzy");

    @Verify
    public boolean verify() throws Exception {
        Matcher matcher = PIC_PATTERN.matcher(RobotDealContext.getContent());
        while (matcher.find()) {
            String picUrl = matcher.group(1);
            TAipVision client = new TAipVision(QQAiUtil.app_id, QQAiUtil.app_key);
            //智能鉴黄ByURL
            String picRes = client.visionPorn(ImageUtil.downloadPic(picUrl));
            if (StringUtils.isNotEmpty(picRes)) {
                JSONObject json = JSON.parseObject(picRes);
                if (json.getInteger("ret") == 0) {
                    JSONArray array = json.getJSONObject("data").getJSONArray("tag_list");
                    /**
                     * tag_name	含义
                     * normal	正常
                     * hot	性感
                     * porn	黄色图像
                     * female-genital	女性阴部
                     * female-breast	女性胸部
                     * male-genital	男性阴部
                     * pubes	阴毛
                     * anus	肛门
                     * sex	性行为
                     * normal_hot_porn	图像为色情的综合值
                     */
                    int normal = 0;
                    int hot = 0;
                    int porn = 0;
                    int female_breast = 0;
                    int female_genital = 0;
                    int male_genital = 0;
                    int pubes = 0;
                    int anus = 0;
                    int sex = 0;
                    int normal_hot_porn = 0;
                    for (JSONObject jsonObject : array.toJavaList(JSONObject.class)) {
                        if ("normal".equals(jsonObject.getString("tag_name"))) {
                            normal = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("porn".equals(jsonObject.getString("tag_name"))) {
                            porn = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("hot".equals(jsonObject.getString("tag_name"))) {
                            hot = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("female-breast".equals(jsonObject.getString("tag_name"))) {
                            female_breast = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("female-genital".equals(jsonObject.getString("tag_name"))) {
                            female_genital = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("male-genital".equals(jsonObject.getString("tag_name"))) {
                            male_genital = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("pubes".equals(jsonObject.getString("tag_name"))) {
                            pubes = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("anus".equals(jsonObject.getString("tag_name"))) {
                            anus = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("sex".equals(jsonObject.getString("tag_name"))) {
                            sex = jsonObject.getIntValue("tag_confidence");
                        }
                        if ("normal-hot-porn".equals(jsonObject.getString("tag_name"))) {
                            normal_hot_porn = jsonObject.getIntValue("tag_confidence");
                        }
                    }
                    if (normal < 40) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    @Do
    public boolean done(){
        String robotQQ = RobotDealContext.getRobotQQ();
        Integer msgType = RobotDealContext.getMsgType();
        Integer subMsgType = RobotDealContext.getSubMsgType();
        String from = RobotDealContext.getFrom();
        String fromQQ = RobotDealContext.getFromQQ();
        if (!MsgType.QUN.getValue().equals(msgType) && !MsgType.GROUP.getValue().equals(msgType)) {
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "答应我,不要再发黄图了好吗?");
        } else {
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n答应我,不要再发黄图了好吗?");
        }
        return true;
    }
}
