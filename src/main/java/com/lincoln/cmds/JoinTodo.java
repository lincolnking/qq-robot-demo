package com.lincoln.cmds;

import com.lincoln.bean.SpringBeanFactoryUtils;
import com.lincoln.entity.todo.DbTodo;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.entity.user.DbUser;
import com.lincoln.service.GroupService;
import com.lincoln.service.TodoService;
import com.lincoln.service.TodoUserService;
import com.lincoln.service.UserService;
import com.lincoln.utils.WebContextUtil;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.GroupMsg;
import com.lincolnXmypcqq.cmds.annotation.QunMsg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.RobotDeal;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * @author ziyao.peng01@ucarinc.com
 * @description 加入活动
 * @date 2018/11/27 15:14
 **/
@GroupMsg(priority = 3)
@QunMsg(priority = 3)
public class JoinTodo {
    public static final String CMD = "加入";

    private UserService userService = SpringBeanFactoryUtils.getBean(UserService.class);
    private GroupService groupService = SpringBeanFactoryUtils.getBean(GroupService.class);
    private TodoService todoService = SpringBeanFactoryUtils.getBean(TodoService.class);
    private TodoUserService todoUserService = SpringBeanFactoryUtils.getBean(TodoUserService.class);
    private WebContextUtil webContextUtil = SpringBeanFactoryUtils.getBean(WebContextUtil.class);

    @Verify
    public boolean verify() {
        if (RobotDealContext.getContent().startsWith(CMD)) {
            return true;
        }
        return false;
    }

    @Do
    public boolean done(){
        String robotQQ = RobotDealContext.getRobotQQ();
        Integer msgType = RobotDealContext.getMsgType();
        Integer subMsgType = RobotDealContext.getSubMsgType();
        String from = RobotDealContext.getFrom();
        String fromQQ = RobotDealContext.getFromQQ();
        String content = RobotDealContext.getContent();
        DbUser user = userService.findByQQ(fromQQ);
        DbGroup group = groupService.findByQQ(from);
        if (CMD.equals(content)) {
            List<DbTodo> todos = todoService.findWaitTodo(group.getId());
            if (todos != null && todos.size() > 0) {
                DbTodo todo = todos.get(0);
                RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n" + todoUserService.joinTodo(user, todo) + ",发送:\"退出" + todo.getId() + "\"即可退出");
                return true;
            }
        }
        //@方式加入他人活动
        if ((content.startsWith(CMD + " [@") || content.startsWith(CMD + "[@")) && content.endsWith("]")) {
            String qq = content.replace(CMD + "[@", "");
            qq = qq.replace(CMD + " [@", "");
            qq = qq.replaceAll("]", "");
            List<DbTodo> todos = todoService.findWaitTodo(qq, group.getId());
            if (todos != null && todos.size() > 0) {
                DbTodo todo = todos.get(0);
                RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n" + todoUserService.joinTodo(user, todo) + ",发送:\"退出" + todo.getId() + "\"即可退出");
                return true;
            }
        }
        if (StringUtils.isNotEmpty(content.replace(CMD, ""))) {
            try {
                long id = Long.parseLong(content.replace(CMD, ""));
                DbTodo todo = todoService.findById(id);
                if (todo != null) {
                    if (todo.getNumber() <= 0) {
                        RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n人已经满了");
                        return true;
                    } else {
                        RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n" + todoUserService.joinTodo(user, todo) + ",发送:\"退出" + todo.getId() + "\"即可退出");
                        return true;
                    }
                }
            } catch (Exception e) {

            }
        }
        return true;
    }
}
