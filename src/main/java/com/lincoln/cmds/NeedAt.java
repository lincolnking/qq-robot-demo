package com.lincoln.cmds;

import com.lincoln.entity.MsgType;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.Msg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;

/**
 * @author ziyao.peng01@ucarinc.com
 * @description 需要@才能继续执行
 * @date 2018/11/29 10:25
 **/
@Msg(priority = 3)
public class NeedAt {
    @Verify
    public boolean verify() throws Exception {
        //判断是不是群消息
        if (MsgType.QUN.getValue().equals(RobotDealContext.getMsgType()) || MsgType.GROUP.getValue().equals(RobotDealContext.getMsgType())) {
            return true;
        } else {
            return false;
        }
    }

    @Do
    public boolean done() {
        if (RobotDealContext.getContent().contains("[@" + RobotDealContext.getRobotQQ() + "]")) {
            return false;
        } else {
            RobotDealContext.setContent(RobotDealContext.getContent().replaceAll("\\[@" + RobotDealContext.getRobotQQ() + "\\]", ""));
            RobotDealContext.setContent(RobotDealContext.getContent().trim());
            return true;
        }
    }
}
