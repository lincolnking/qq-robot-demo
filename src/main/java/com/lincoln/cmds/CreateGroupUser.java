package com.lincoln.cmds;

import com.lincoln.bean.SpringBeanFactoryUtils;
import com.lincoln.entity.user.*;
import com.lincoln.entity.MsgType;
import com.lincoln.service.*;
import com.lincoln.utils.WebContextUtil;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.Msg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;

/**
 * @author ziyao.peng01@ucarinc.com
 * @description 前置生成数据库中没有的用户信息, 群信息, 群用户信息
 * @date 2018/11/29 11:10
 **/
@Msg(priority = 2)
public class CreateGroupUser {
    UserService userService;
    GroupService groupService;
    GroupUserService groupUserService;
    TodoService todoService;
    TodoUserService todoUserService;
    WebContextUtil webContextUtil;

    @Verify
    public boolean verify() {
        return true;
    }

    @Do
    public boolean done() {
        this.userService = SpringBeanFactoryUtils.getBean(UserService.class);
        this.groupService = SpringBeanFactoryUtils.getBean(GroupService.class);
        this.groupUserService = SpringBeanFactoryUtils.getBean(GroupUserService.class);
        this.todoService = SpringBeanFactoryUtils.getBean(TodoService.class);
        this.todoUserService = SpringBeanFactoryUtils.getBean(TodoUserService.class);
        this.webContextUtil = SpringBeanFactoryUtils.getBean(WebContextUtil.class);

        String robotQQ = RobotDealContext.getRobotQQ();
        Integer msgType = RobotDealContext.getMsgType();
        Integer subMsgType = RobotDealContext.getSubMsgType();
        String from = RobotDealContext.getFrom();
        String fromQQ = RobotDealContext.getFromQQ();
        String content = RobotDealContext.getContent();

        DbUser robot = userService.findByQQ(robotQQ);
        if (robot == null) {
            robot = new DbUser();
            robot.setQq(robotQQ);
            robot.setIdentity(UserIdentityEnum.ROBOT);
            userService.save(robot);
        }
        DbUser user = userService.findByQQ(fromQQ);
        if (user == null) {
            user = new DbUser();
            user.setQq(fromQQ);
            user.setIdentity(UserIdentityEnum.USER);
            userService.save(user);
        }
        //群消息
        if (MsgType.GROUP.getValue().equals(msgType) || MsgType.QUN.getValue().equals(msgType)) {
            DbGroup group = groupService.findByQQ(from);
            if (group == null) {
                group = new DbGroup();
                group.setGroupNo(from);
                if (MsgType.GROUP.getValue().equals(msgType)) {
                    group.setGroupType(GroupTypeEnum.GROUP);
                }
                if (MsgType.QUN.getValue().equals(msgType)) {
                    group.setGroupType(GroupTypeEnum.QUN);
                }
                group = groupService.save(group);
            }
            DbGroupUser robotUser = groupUserService.findByUserIdAndGroupId(robot.getId(), group.getId());
            if (robotUser == null) {
                robotUser = new DbGroupUser();
                robotUser.setUser(robot);
                robotUser.setGroup(group);
                robotUser = groupUserService.save(robotUser);
            }
            DbGroupUser groupUser = groupUserService.findByUserIdAndGroupId(user.getId(), group.getId());
            if (groupUser == null) {
                groupUser = new DbGroupUser();
                groupUser.setUser(user);
                groupUser.setGroup(group);
                groupUser = groupUserService.save(groupUser);
            }
        }
        return true;
    }
}
