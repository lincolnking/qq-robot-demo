package com.lincoln.cmds;

import com.lincoln.bean.SpringBeanFactoryUtils;
import com.lincoln.entity.todo.DbTodo;
import com.lincoln.entity.todo.DbTodoUser;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.entity.user.DbUser;
import com.lincoln.service.GroupService;
import com.lincoln.service.TodoService;
import com.lincoln.service.TodoUserService;
import com.lincoln.service.UserService;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.GroupMsg;
import com.lincolnXmypcqq.cmds.annotation.QunMsg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.RobotDeal;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * @author ziyao.peng01@ucarinc.com
 * @description 退出活动
 * @date 2018/11/27 15:14
 **/
@GroupMsg(priority = 4)
@QunMsg(priority = 4)
public class ExitTodo {
    public static final String CMD = "退出";

    private UserService userService = SpringBeanFactoryUtils.getBean(UserService.class);
    private GroupService groupService = SpringBeanFactoryUtils.getBean(GroupService.class);
    private TodoService todoService = SpringBeanFactoryUtils.getBean(TodoService.class);
    private TodoUserService todoUserService = SpringBeanFactoryUtils.getBean(TodoUserService.class);

    @Verify
    public boolean verify() {
        if (RobotDealContext.getContent().startsWith(CMD)) {
            return true;
        }
        return false;
    }

    @Do
    public boolean done() {
        String robotQQ = RobotDealContext.getRobotQQ();
        Integer msgType = RobotDealContext.getMsgType();
        Integer subMsgType = RobotDealContext.getSubMsgType();
        String from = RobotDealContext.getFrom();
        String fromQQ = RobotDealContext.getFromQQ();
        String content = RobotDealContext.getContent();
        DbUser user = userService.findByQQ(fromQQ);
        DbGroup group = groupService.findByQQ(from);
        if (content.equals(CMD)) {
            List<DbTodoUser> todoUsers = todoUserService.findByUserIdAndGroupId(user.getId(), group.getId(), 3);
            if (todoUsers != null && todoUsers.size() > 0) {
                DbTodoUser todoUser = todoUsers.get(0);
                DbTodo todo = todoUser.getTodo();
                if (todoUserService.escTodo(todoUsers.get(0))) {
                    RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n\"" + todo.getTitle() + "\"退出成功");
                    return true;
                }
            } else {
                RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n\"" + "你没有活动了!");
            }
        }
        //@方式加入他人活动
        if ((content.startsWith(CMD + " [@") || content.startsWith(CMD + "[@")) && content.endsWith("]")) {
            String qq = content.replace(CMD + "[@", "");
            qq = qq.replace(CMD + " [@", "");
            qq = qq.replaceAll("]", "");
            DbUser todoUser = userService.findByQQ(qq);
            List<DbTodoUser> todoUsers = todoUserService.findByUserIdAndGroupIdAndTodoUserId(user.getId(), todoUser.getId(), group.getId());
            if (todoUsers != null && todoUsers.size() > 0) {
                DbTodo todo = todoUsers.get(0).getTodo();
                if (todoUserService.escTodo(todoUsers.get(0))) {
                    RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n\"" + todo.getTitle() + "\"退出成功");
                    return true;
                }
            } else {
                RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n\"" + "你没有活动了!");
            }
        }
        if (StringUtils.isNotEmpty(content.replace(CMD, ""))) {
            try {
                long id = Long.parseLong(content.replace(CMD, ""));
                DbTodo todo = todoService.findById(id);
                if (todo != null) {
                    DbTodoUser todoUser = todoUserService.findByUserIdAndTodoId(user.getId(), todo.getId());
                    if (todoUser != null) {
                        if (todoUserService.escTodo(todoUser)) {
                            RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n\"" + todo.getTitle() + "\"退出成功");
                            return true;
                        }
                    }
                } else {
                    RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n\"" + "没这个活动!");
                }
            } catch (Exception e) {

            }
        }
        return true;
    }
}
