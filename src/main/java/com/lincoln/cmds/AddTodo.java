package com.lincoln.cmds;

import com.lincoln.bean.SpringBeanFactoryUtils;
import com.lincoln.entity.todo.DbTodo;
import com.lincoln.entity.todo.TodoStatusEnum;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.entity.user.DbUser;
import com.lincoln.utils.TimerMap;
import com.lincoln.service.GroupService;
import com.lincoln.service.TodoService;
import com.lincoln.service.UserService;
import com.lincoln.utils.WebContextUtil;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.GroupMsg;
import com.lincolnXmypcqq.cmds.annotation.QunMsg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.RobotDeal;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;

import java.util.Date;

/**
 * @author ziyao.peng01@ucarinc.com
 * @description 新增活动
 * @date 2018/11/27 15:14
 **/
@GroupMsg(priority = 1)
@QunMsg(priority = 1)
public class AddTodo {
    public static final String CMD = "新增活动";

    private UserService userService = SpringBeanFactoryUtils.getBean(UserService.class);
    private GroupService groupService = SpringBeanFactoryUtils.getBean(GroupService.class);
    private TodoService todoService = SpringBeanFactoryUtils.getBean(TodoService.class);
    private WebContextUtil webContextUtil = SpringBeanFactoryUtils.getBean(WebContextUtil.class);

    @Verify
    public boolean verify() {
        if (RobotDealContext.getContent().startsWith(CMD)) {
            return true;
        }
        return false;
    }

    @Do
    public boolean done() {
        String robotQQ = RobotDealContext.getRobotQQ();
        Integer msgType = RobotDealContext.getMsgType();
        Integer subMsgType = RobotDealContext.getSubMsgType();
        String from = RobotDealContext.getFrom();
        String fromQQ = RobotDealContext.getFromQQ();
        String content = RobotDealContext.getContent();
        content = content.substring(CMD.length());
        DbUser user = userService.findByQQ(fromQQ);
        DbGroup group = groupService.findByQQ(from);
        DbTodo todo = new DbTodo();
        todo.setUser(user);
        todo.setGroup(group);
        todo.setTitle(content.length() > 20 ? content.substring(0, 20) : content);
        todo.setDescriptor(content);
        todo.setStartTime(new Date(System.currentTimeMillis() + 2 * 60 * 60 * 1000));
        todo.setEndTime(new Date(System.currentTimeMillis() + 3 * 60 * 60 * 1000));
        todo.setHint("[@" + fromQQ + "]创建的活动马上就要开始了,赶紧就位[@]");
        todo.setStatus(TodoStatusEnum.WATI);
        todo.setNumber(5);
        todoService.save(todo);
        TimerMap.addTimer(todo);
        RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]\\n创建好了,默认5人在两小时后开始持续一小时,你可以在这个url设置详细" + webContextUtil.getHost() + "/setTodo.html?id=" + todo.getId());
        return true;
    }
}
