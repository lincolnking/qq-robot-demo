package com.lincoln.cmds;

import com.lincoln.bean.SpringBeanFactoryUtils;
import com.lincoln.entity.todo.DbTodo;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.service.GroupService;
import com.lincoln.service.TodoService;
import com.lincolnXmypcqq.cmds.annotation.Do;
import com.lincolnXmypcqq.cmds.annotation.GroupMsg;
import com.lincolnXmypcqq.cmds.annotation.QunMsg;
import com.lincolnXmypcqq.cmds.annotation.Verify;
import com.lincolnXmypcqq.robot.RobotDeal;
import com.lincolnXmypcqq.robot.framework.RobotDealContext;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author ziyao.peng01@ucarinc.com
 * @description 有啥活动
 * @date 2018/11/27 15:14
 **/
@GroupMsg(priority = 2)
@QunMsg(priority = 2)
public class ListTodo {
    public static final String CMD = "有啥活动";

    private GroupService groupService = SpringBeanFactoryUtils.getBean(GroupService.class);
    private TodoService todoService = SpringBeanFactoryUtils.getBean(TodoService.class);

    @Verify
    public boolean verify() {
        if (RobotDealContext.getContent().startsWith(CMD)) {
            return true;
        }
        return false;
    }

    @Do
    public boolean done(){
        String robotQQ = RobotDealContext.getRobotQQ();
        Integer msgType = RobotDealContext.getMsgType();
        Integer subMsgType = RobotDealContext.getSubMsgType();
        String from = RobotDealContext.getFrom();
        String fromQQ = RobotDealContext.getFromQQ();
        DbGroup group = groupService.findByQQ(from);
        List<DbTodo> todos = todoService.findWaitTodo(group.getId());
        String msg = "\\n";
        for (DbTodo todo : todos) {
            msg += "id:" + todo.getId() + "-时间:" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(todo.getStartTime()) + "-" + todo.getTitle() + "-剩余:" + todo.getNumber() + "人\\n";
        }
        msg += "回复:加入+ID号即可加入";
        RobotDeal.sendMsg(robotQQ, msgType, subMsgType, from, fromQQ, "[@" + fromQQ + "]" + msg);
        return true;
    }
}
