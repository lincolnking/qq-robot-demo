package com.lincoln;

import com.lincoln.repository.base.BaseRepositoryFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan({"com.lincoln.entity.*.*", "com.lincoln.entity"})
@EnableJpaRepositories(basePackages = {"com.lincoln.repository"},
        repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class//指定自己的工厂类
)
@ComponentScan(basePackages = {"com.lincolnXmypcqq.*","com.lincoln.*"})
public class QqRobotApplication {
    public static void main(String[] args) {
        SpringApplication.run(QqRobotApplication.class, args);
    }
}
