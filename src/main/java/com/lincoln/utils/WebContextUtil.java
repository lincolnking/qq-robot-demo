package com.lincoln.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Data
@Component
public class WebContextUtil {
    @Value("${host}")
    String host;
}
