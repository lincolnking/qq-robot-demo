package com.lincoln.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * md5工具
 */
public class MD5 {

    public static String hexMd5(String s) {
        MessageDigest md5Dig = null;
        try {
            md5Dig = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException e) {
            // do nothing , it is impossible!!

        }
        byte[] bytes = s.getBytes();
        md5Dig.update(bytes);
        return byte2hex(md5Dig.digest());
    }

    private static String byte2hex(byte[] bytes) {
        StringBuffer buf = new StringBuffer("");
        for (int i = 0; i < bytes.length; i++) {
            String t = Integer.toHexString(bytes[i] >= 0 ? bytes[i] : (bytes[i] + 256));
            if (t.length() < 2) {
                t = "0" + t;
            }
            buf.append(t.toUpperCase());
        }
        return buf.toString();
    }
}
