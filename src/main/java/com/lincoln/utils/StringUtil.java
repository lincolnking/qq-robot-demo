package com.lincoln.utils;

public class StringUtil {
    /**
     * 找出sub在source中出现的次数
     * @param source
     * @param sub
     * @return
     */
    public static int getCount(String source, String sub) {
        int count = 0;
        int length = source.length() - sub.length();
        for (int i = 0; i < length; i++) {
            String sourceBak = source.substring(i, i + sub.length());
            int index = sourceBak.indexOf(sub);
            if (index != -1) {
                count++;
            }
        }
        return count;
    }


}
