package com.lincoln.utils;

import com.lincoln.bean.SpringBeanFactoryUtils;
import com.lincoln.entity.MsgType;
import com.lincoln.entity.todo.DbTodo;
import com.lincoln.entity.todo.DbTodoUser;
import com.lincoln.entity.todo.TodoStatusEnum;
import com.lincoln.entity.user.DbGroup;
import com.lincoln.entity.user.DbGroupUser;
import com.lincoln.entity.user.DbUser;
import com.lincoln.entity.user.GroupTypeEnum;
import com.lincoln.framework.bean.NettyContext;
import com.lincoln.service.GroupUserService;
import com.lincoln.service.TodoService;
import com.lincoln.service.TodoUserService;
import com.lincolnXmypcqq.netty.DealChat;
import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class TimerMap {
    public static Map<Long, Timer> timerMap = new ConcurrentHashMap<>();

    public static class TodoTimerTask extends TimerTask {
        private long id;

        public TodoTimerTask(long id) {
            this.id = id;
        }

        @Override
        public void run() {
            TodoUserService todoUserService = SpringBeanFactoryUtils.getBean(TodoUserService.class);
            TodoService todoService = SpringBeanFactoryUtils.getBean(TodoService.class);
            List<DbTodoUser> todoUsers = todoUserService.findByTodoId(id);
            if (todoUsers != null && todoUsers.size() > 0) {
                DbTodo todo = todoUsers.get(0).getTodo();
                DbGroup group = todo.getGroup();
                GroupUserService groupUserService = SpringBeanFactoryUtils.getBean(GroupUserService.class);
                List<DbGroupUser> groupUsers = groupUserService.findGroupRobot(group.getId());
                if (groupUsers != null && groupUsers.size() > 0) {
                    DbUser robot = groupUsers.get(0).getUser();
                    String msg = "";
                    msg += todoUsers.get(0).getTodo().getHint();
                    if (msg.indexOf("[@]") != -1) {
                        String aite = "\\\\n";
                        for (DbTodoUser todoUser : todoUsers) {
                            aite += "[@" + todoUser.getUser().getQq() + "] ";
                        }
                        msg = msg.replaceAll("\\[@\\]", aite);
                    }
                    if (StringUtils.isNotEmpty(msg)) {
                        int msgType = MsgType.GROUP.getValue();
                        if (group.getGroupType() == GroupTypeEnum.QUN) {
                            msgType = MsgType.QUN.getValue();
                        }
                        sendMsg(robot.getQq(), msgType, 0, group.getGroupNo(), group.getGroupNo(), msg);
                    }
                }
                todo = todoService.findById(todo.getId());
                todo.setStatus(TodoStatusEnum.BEGIN);
                todoService.save(todo);
            }
        }

        public void sendMsg(String robotQQ, int msgType, int subMsgType, String to, String toQQ, String content) {
            if (NettyContext.getClient(robotQQ) != null) {
                DealChat dealChat = new DealChat();
                dealChat.send(robotQQ, String.valueOf(msgType), String.valueOf(subMsgType), to, toQQ, content);
            }
        }
    }

    public static boolean addTimer(DbTodo todo) {
        if (todo.getEndTime() == null || todo.getEndTime().getTime() <= todo.getStartTime().getTime()) {
            todo.setEndTime(new Date(todo.getStartTime().getTime() + 1 * 60 * 60 * 1000));
        }
        if (todo.getStartTime().getTime() <= System.currentTimeMillis()) {
            if (todo.getEndTime().getTime() >= System.currentTimeMillis()) {
                todo.setStatus(TodoStatusEnum.END);
            } else {
                todo.setStatus(TodoStatusEnum.BEGIN);
            }
        } else {
            todo.setStatus(TodoStatusEnum.WATI);
        }
        TodoService todoService = SpringBeanFactoryUtils.getBean(TodoService.class);
        todo = todoService.save(todo);
        if (todo.getStatus() == TodoStatusEnum.END) {
            return false;
        }
        TodoTimerTask todoTimerTask = new TodoTimerTask(todo.getId());
        Timer timer = new Timer(true);
        timer.schedule(todoTimerTask, todo.getStartTime());
        timerMap.put(todo.getId(), timer);
        return true;
    }

    public static boolean removeTimer(DbTodo todo) {
        Timer timer = timerMap.get(todo.getId());
        if (timer == null) {
            return false;
        } else {
            timer.cancel();
            timerMap.remove(todo.getId());
            return true;
        }
    }
}
