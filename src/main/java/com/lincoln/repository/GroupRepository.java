package com.lincoln.repository;

import com.lincoln.entity.user.DbGroup;
import com.lincoln.repository.base.BaseDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface GroupRepository extends BaseDao<DbGroup> {
}
