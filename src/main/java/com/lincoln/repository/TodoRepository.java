package com.lincoln.repository;

import com.lincoln.entity.todo.DbTodo;
import com.lincoln.repository.base.BaseDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface TodoRepository extends BaseDao<DbTodo> {
}
