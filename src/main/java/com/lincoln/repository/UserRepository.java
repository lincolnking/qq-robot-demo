package com.lincoln.repository;

import com.lincoln.entity.user.DbUser;
import com.lincoln.repository.base.BaseDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(rollbackFor = Exception.class)
public interface UserRepository extends BaseDao<DbUser> {
}
