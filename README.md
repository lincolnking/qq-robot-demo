# QQ机器人SDK

#### 项目介绍
qq-robot-sdk的demo,引用[qq-robot-sdk](https://gitee.com/lincolnking/qq-robot-sdk)

#### 软件架构
使用springboot来接收来自mypcqq中模块的请求;
使用netty实现向mypcqq客户端发送命令,通过[基于netty的物联网通讯协议服务端实现](https://gitee.com/lincolnking/netty-server)实现
springboot开发使用了[基于Springboot的jpa快速开发工具类](https://gitee.com/lincolnking/lincoln-springboot-jpa-starter)进行快速开发

#### 安装教程

1. git clone下来
2. maven import
3. 启动springboot
4. 安装易语言、mypcqq
5. 将[qq-robot-sdk](https://gitee.com/lincolnking/qq-robot-sdk)中的转发Java.e编译后,放入mypcqq插件文件夹
6. 启动mypcqq后,激活插件,在配置窗口中配置java服务器地址端口即可

#### 使用说明

1. xxxx
2. xxxx
3. xxxx